
export default class Bloc extends createjs.Shape {

    constructor(taille = 20) {
        super();
        this.taille = taille;
        //this.chargeur = chargeur;
        this.initialiser();
        // this.fSoumtime()
    }

    initialiser() {

        this.graphics
            .setStrokeStyle(5, "round")
            .beginStroke("red")
            .beginFill("black") // nécessaire pour la détection de collision
            .drawRect(0, 0, 600, this.taille);

        this.cache(-5, -5, 1000, this.taille + 5 * 2);

    }


    // fSoumtime(){
    //
    //     let soumtime;
    //
    //     soumtime = new Soumtime(this.chargeur);
    //     soumtime.x = this.x - 50;
    //     soumtime.y = this.y;
    //     let index = stage.getChildIndex(this);
    //     this.stage.addChild(soumtime, index);
    //
    // }

}