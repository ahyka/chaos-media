export default class Poisson2 extends createjs.Sprite{
    constructor(chargeur){
        super(chargeur.getResult("poisson" + Math.round(Math.random()*4+1)));

        this.scaleX = -0.3;
        this.scaleY = 0.3;
        this.x = 2200;
        this.y = Math.random()*700;

        this.gotoAndPlay("animations");

        createjs.Tween
            .get(this)
            .to({y: this.y+10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y-10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y+10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y-10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y+10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y-10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y+10}, 3000, createjs.Ease.quadInOut)
            .to({y: this.y-10}, 3000, createjs.Ease.quadInOut);

    }

    enleverPoisson(animeVentou){

        //this.animeVentou = animeVentou;

        //this.animeVentou.normal();

        createjs.Sound.play("pop", {"volume": 0.5});

        this.parent.removeChild(this);

    }

    replacerPoissSurVentouse(ventouse){

        this.pVentouse = ventouse;

        this.poX = this.x - this.pVentouse.ventou.x;
        this.poY = this.y - this.pVentouse.ventou.y;

        setInterval(() => {

            this.x = this.pVentouse.ventou.x + this.poX;
            this.y = this.pVentouse.ventou.y + this.poY;

        }, 0);

    }
}