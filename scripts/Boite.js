export default class Boite extends createjs.Bitmap{
    constructor(chargeur){
        super(chargeur.getResult("boite" + Math.round(Math.random()*5+1)));

        this.taille = Math.random()*0.3+0.3;

        this.scaleY = this.taille;
        this.scaleX = this.taille;

        this.x = -1000;
        this.y = Math.random()*500;

        createjs.Tween
            .get(this)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut);



    }

    enleverBoite(animeVentou){

        this.animeVentou = animeVentou;

        //this.animeVentou.normal();

        createjs.Sound.play("pop", {"volume": 0.5});

        this.parent.removeChild(this);
    }

    replacerBoiteSurVentouse(ventouse){

        this.pVentouse = ventouse;

        this.poX = this.x - this.pVentouse.ventou.x;
        this.poY = this.y - this.pVentouse.ventou.y;

        setInterval(() => {

            this.x = this.pVentouse.ventou.x + this.poX;
            this.y = this.pVentouse.ventou.y + this.poY;

        }, 0);

    }

}