import Bombe from './Bombe.js';
import Bombe2 from './Bombe2.js';
import Boite from './Boite.js';
import Boite2 from './Boite2.js';
import Poisson from './Poisson.js';
import Poisson2 from './Poisson2.js';

//import Decor from './decor.js';

import BatonD from './BatonD.js';
import BatonG from './batonG.js';

let gamepad = new Gamepad();
let gamepad2 = new Gamepad();

gamepad.setCustomMapping('keyboard', {
    'button_1': 88
});

gamepad2.setCustomMapping('keyboard', {
    'button_2': 90
});

export default class Jeu {

    constructor() {
        // Manifest
        this.chargeur = new createjs.LoadQueue();
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.loadManifest("ressources/manifest.json");

        this.chargeur.addEventListener('error', e => alert('Error with ' + e));
        this.chargeur.addEventListener('complete', () => this.initialiser());

        // Référence au canevas dans le DOM
        this.canvas = document.querySelector("canvas");
    }

    initialiser() {

        // Ajustement du canevas à la taille de la fenêtre
        this.canvas.width = 1920;
        this.canvas.height = 1080;

        // Préparation du StageGL
        this.stage = new createjs.StageGL(this.canvas);

        //tirer
        this.peutTirerRed = true;
        this.peutTirerBlue = true;

        this.colRed = false;
        this.colBlue = false;

        this.onetimeRed = false;
        this.onetimeBlue = false;

        this.boutonRejouer = false;

        // Préparation du Ticker
        createjs.Ticker.addEventListener("tick", e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;

        this.nombreAttrapes = 0;
        this.temps = 0;

        this.ajouterDecors();

        // bouton pour débuter
        this.boutonDebut = new createjs.Bitmap(this.chargeur.getResult("boutonCommencer"));
        this.boutonDebut.scaleY = 0.7;
        this.boutonDebut.scaleX = 0.7;
        this.boutonDebut.x = (this.stage.canvas.width - this.boutonDebut.getBounds().width * 0.7) / 2;
        this.boutonDebut.y = 450;
        this.boutonDebut.alpha = 0.1;
        this.stage.addChild(this.boutonDebut);

        this.boutonDebut.addEventListener('click', () => document.fonts.load('48px \"bluefish\"').then(() => this.debuter()));

        //document.addEventListener('keydown', this.teste.bind(this));

        this.texteDebut = new createjs.Text('Appuyez sur la gâchette pour commencer', '50px black', 'white');
        this.texteDebut.cache(0, 0, 1200, 600);
        this.texteDebut.x = 400;
        this.texteDebut.y = 200;
        this.stage.addChild(this.texteDebut);

        createjs.Tween
            .get(this.texteDebut, {loop:true})
            .to({alpha: 0.3}, 600)
            .to({alpha: 1}, 1000)
            .to({alpha: 0.5}, 600)
            .to({alpha: 1}, 1000);

        this.joueurPasPret1 = new createjs.Bitmap(this.chargeur.getResult("pasPret"));
        this.joueurPasPret1.x = 280;
        this.joueurPasPret1.y = 350;
        this.joueurPasPret1.scaleX = 0.5;
        this.joueurPasPret1.scaleY = 0.5;
        this.stage.addChild(this.joueurPasPret1);


        this.joueurPasPret2 = new createjs.Bitmap(this.chargeur.getResult("pasPret"));
        this.joueurPasPret2.x = 1300;
        this.joueurPasPret2.y = 350;
        this.joueurPasPret2.scaleX = 0.5;
        this.joueurPasPret2.scaleY = 0.5;
        this.stage.addChild(this.joueurPasPret2);

        this.start1 = false;
        this.start2 = false;

        this.joueurPret1 = new createjs.Bitmap(this.chargeur.getResult("pret"));
        this.joueurPret1.x = 342.5;
        this.joueurPret1.y = 350;
        this.joueurPret1.scaleX = 0.5;
        this.joueurPret1.scaleY = 0.5;
        this.stage.addChild(this.joueurPret1);

        this.joueurPret1.alpha = 0;

        this.joueurPasPret1.alpha = 1;

        this.joueurPret2 = new createjs.Bitmap(this.chargeur.getResult("pret"));
        this.joueurPret2.x = 1362;
        this.joueurPret2.y = 350;
        this.joueurPret2.scaleX = 0.5;
        this.joueurPret2.scaleY = 0.5;
        this.stage.addChild(this.joueurPret2);

        this.joueurPret2.alpha = 0;

        this.joueurPasPret2.alpha = 1;

        gamepad.on('press', 'button_1', e => {

            if (e.button == 'button_1' && this.start1 === false){

                this.start1 = true;

                this.joueurPret1.alpha = 1;
                this.joueurPasPret1.alpha = 0;

                // this.textePret1.text = 'Pret';
                // this.textePret1.updateCache()

            } else {

                this.start1 = false;

                this.joueurPret1.alpha = 0;
                this.joueurPasPret1.alpha = 1;

                // this.textePret1.text = 'Pas pret';
                // this.textePret1.updateCache()

            }

            if (this.start1 === true && this.start2 === true){

                this.stage.removeChild(this.texteDebut);
                this.stage.removeChild(this.joueurPret1);
                this.stage.removeChild(this.joueurPret2);
                this.stage.removeChild(this.joueurPasPret1);
                this.stage.removeChild(this.joueurPasPret2);
                this.debuter()

            }

        });

        gamepad2.on('press', 'button_2', e => {

            if (e.button == 'button_2' && this.start2 === false){

                this.start2 = true;

                this.joueurPret2.alpha = 1;
                this.joueurPasPret2.alpha = 0;

                // this.textePret2.text = 'Pret';
                // this.textePret2.updateCache()

            } else {

                this.start2 = false;

                this.joueurPret2.alpha = 0;
                this.joueurPasPret2.alpha = 1;

                // this.textePret2.text = 'Pas pret';
                // this.textePret2.updateCache()

            }

            if (this.start1 === true && this.start2 === true){

                this.stage.removeChild(this.texteDebut);
                this.stage.removeChild(this.joueurPret1);
                this.stage.removeChild(this.joueurPret2);
                this.stage.removeChild(this.joueurPasPret1);
                this.stage.removeChild(this.joueurPasPret2);
                this.debuter()

            }

        });

        // musique
        createjs.Sound.play("musique", {"loop": -1, "volume": 0.5});
    }

    debuter(e) {

        this.cadran();
        this.fusil();

        // this.textePret1.alpha = 0;
        // this.textePret2.alpha = 0;

        //Écouteur sur le Ticker pour actualiser
        this.eActualiser = this.actualiser.bind(this);
        createjs.Ticker.addEventListener('tick', this.eActualiser);

        // enlever bouton pour débuter
        this.stage.removeChild(this.boutonDebut);

        // éléments du décor
        // 1
        // this.conteneurFond1Element = new createjs.Container();
        // this.stage.addChild(this.conteneurFond1Element);
        //
        // this.intervalFond1Element = setInterval(() => {
        //     this.fond1Elementhauteur = Math.random() * 50 + 740;
        //     this.fond1Element = new Decor(this.chargeur);
        //     this.fond1Element.y = this.fond1Elementhauteur;
        //     this.conteneurFond1Element.addChild(this.fond1Element);
        // }, 4000);
        //
        // // 2
        // this.conteneurFond2Element = new createjs.Container();
        // this.stage.addChild(this.conteneurFond2Element);
        //
        // this.intervalFond2Element = setInterval(() => {
        //     this.fond2Elementhauteur = Math.random() * 150 + 520;
        //     this.fond2Element = new Decor(this.chargeur);
        //     this.fond2Element.alpha = 0.7;
        //     this.fond2Element.scaleX = 0.3;
        //     this.fond2Element.scaleY = 0.3;
        //     this.fond2Element.y = this.fond2Elementhauteur;
        //     this.conteneurFond2Element.addChild(this.fond2Element);
        // }, 6000);


        // decor bouge
        // this.intervalDecor = setInterval(() => {
        //     this.debrisAvion.x += 0.5;
        //     this.decor1.x += 0.5;
        //     this.fond1Element1.x += 0.5;
        //     this.fond1Element2.x += 0.5;
        //     this.fond1Element3.x += 0.5;
        //     this.decor2.x += 0.25;
        //     this.fond2Element1.x += 0.25;
        //     this.fond2Element2.x += 0.25;
        //     this.fond2Element3.x += 0.25;
        //     this.decor3.x += 0.1;
        //
        //     if (this.fond1Element1.x > 1920) {
        //         this.stage.removeChild(this.fond1Element1);
        //     }
        //
        //     if (this.fond1Element2.x > 1920) {
        //         this.stage.removeChild(this.fond1Element2);
        //     }
        //
        //     if (this.fond1Element3.x > 1920) {
        //         this.stage.removeChild(this.fond1Element3);
        //     }
        //
        //     if (this.fond2Element1.x > 1920) {
        //         this.stage.removeChild(this.fond2Element1);
        //     }
        //
        //     if (this.fond2Element2.x > 1920) {
        //         this.stage.removeChild(this.fond2Element2);
        //     }
        //
        //     if (this.fond2Element3.x > 1920) {
        //         this.stage.removeChild(this.fond2Element3);
        //     }
        //
        //
        //     this.conteneurFond1Element.children.forEach(function (e) {
        //         e.x += 0.5;
        //
        //         if (e.x > 1920) {
        //             e.parent.removeChild(e);
        //         }
        //     });
        //
        //     this.conteneurFond2Element.children.forEach(function (e) {
        //         e.x += 0.25;
        //
        //         if (e.x > 1920) {
        //             e.parent.removeChild(e);
        //         }
        //     });
        //
        // }, 0);

        // nombre d'objets attrapés
        this.scoreActuel = new createjs.Bitmap(this.chargeur.getResult("score"));
        this.scoreActuel.y = 12;
        this.scoreActuel.x = 5;
        this.stage.addChild(this.scoreActuel);

        this.textePointage = new createjs.Text(this.nombreAttrapes, '60px \'bluefish\'', 'white');
        this.textePointage.cache(0, 0, 100, 300);
        this.textePointage.x = 455;
        this.textePointage.y = 55;
        this.stage.addChild(this.textePointage);

        this.conteneurBombes = new createjs.Container();
        this.stage.addChild(this.conteneurBombes);

        this.conteneurBombes2 = new createjs.Container();
        this.stage.addChild(this.conteneurBombes2);

        this.conteneurBoites = new createjs.Container();
        this.stage.addChild(this.conteneurBoites);

        this.conteneurBoites2 = new createjs.Container();
        this.stage.addChild(this.conteneurBoites2);

        this.conteneurPoissons = new createjs.Container();
        this.stage.addChild(this.conteneurPoissons);

        this.conteneurPoissons2 = new createjs.Container();
        this.stage.addChild(this.conteneurPoissons2);

        this.intervalBombe = setInterval(() => {
            this.bombe = new Bombe(this.chargeur);
            this.conteneurBombes.addChild(this.bombe);
        }, 9000);

        this.intervalBombe2 = setInterval(() => {
            this.bombe = new Bombe2(this.chargeur);
            this.conteneurBombes2.addChild(this.bombe);
        }, 7000);

        this.intervalBoite = setInterval(() => {
            this.boite = new Boite(this.chargeur, this.textePointage, this.nombreAttrapes);
            this.conteneurBoites.addChild(this.boite);
        }, 3000);

        this.intervalBoite2 = setInterval(() => {
            this.boite = new Boite2(this.chargeur, this.textePointage, this.nombreAttrapes);
            this.conteneurBoites2.addChild(this.boite);
        }, 4500);

        this.intervalPoisson = setInterval(() => {
            this.poisson = new Poisson(this.chargeur);
            this.conteneurPoissons.addChild(this.poisson);
        }, 2000);

        this.intervalPoisson2 = setInterval(() => {
            this.poisson2 = new Poisson2(this.chargeur);
            this.conteneurPoissons2.addChild(this.poisson2);
        }, 2000);

    }

    cadran() {
        this.barreCouleur = new createjs.Bitmap(this.chargeur.getResult("barre1"));
        this.barreCouleur.scaleY = 0.4;
        this.barreCouleur.scaleX = 0.4;
        this.barreCouleur.x = this.stage.canvas.width - this.barreCouleur.getBounds().width * 0.4 + 45;
        this.stage.addChild(this.barreCouleur);

        this.barreBlanche = new createjs.Bitmap(this.chargeur.getResult("barre2"));
        this.barreBlanche.scaleY = 0.4;
        this.barreBlanche.scaleX = 0;
        this.barreBlanche.alpha = 0.7;
        this.barreBlanche.regX = 1500;
        this.barreBlanche.x = this.stage.canvas.width - 120;
        this.stage.addChild(this.barreBlanche);


        this.barre = new createjs.Bitmap(this.chargeur.getResult("barre"));
        this.barre.scaleY = 0.4;
        this.barre.scaleX = 0.4;
        this.barre.x = this.stage.canvas.width - this.barre.getBounds().width * 0.4 + 45;
        this.stage.addChild(this.barre);

        this.sousMarin = new createjs.Bitmap(this.chargeur.getResult("sousMarin"));
        this.sousMarin.scaleY = 0.3;
        this.sousMarin.scaleX = 0.3;
        this.sousMarin.rotation = -30;
        this.sousMarin.x = this.stage.canvas.width - this.sousMarin.getBounds().width * 0.3 - 110;
        this.sousMarin.y = 80;
        this.stage.addChild(this.sousMarin);

        this.intervalTemps = setInterval(this.timer.bind(this), 1000);
    }

    timer() {

        this.deplacement = (this.barre.getBounds().width * 0.4 - this.sousMarin.getBounds().width * 0.3 - 250) / 120;
        this.grandeurBlanc = 0.35 / 120;

        this.sousMarin.x -= this.deplacement;
        this.barreBlanche.scaleX += this.grandeurBlanc;
        this.temps += 1;

        //--------------Terminer le jeu

        if (this.temps === 120) {

            this.deplacement = 0;
            this.grandeurBlanc = 0;
            clearInterval(this.intervalTemps);
            clearInterval(this.intervalBombe);
            clearInterval(this.intervalBombe2);
            clearInterval(this.intervalBoite);
            clearInterval(this.intervalBoite2);
            clearInterval(this.intervalPoisson);
            clearInterval(this.intervalPoisson2);
            //clearInterval(this.intervalDecor);
            createjs.Sound.play("succes", {"volume": 1});

            setTimeout(() => this.ecranDeFin(), 8000);

        }

    }

    ecranDeFin(){

        this.panelFin = new createjs.Bitmap(this.chargeur.getResult('sousMarinPanel'));
        this.stage.addChild(this.panelFin);
        //this.panelFin.x = this.stage.canvas.width / 2 - this.panelFin.getBounds().width / 2;
        this.panelFin.y = 250;
        this.panelFin.x = 2000;

        this.panelScore = new createjs.Bitmap(this.chargeur.getResult('scorePanel'));
        this.stage.addChild(this.panelScore);

        this.panelScore.y = 250;
        this.panelScore.x = 2800;

        this.panelScore.scaleX = 0.7;
        this.panelScore.scaleY = 0.7;

        this.intervalFinPanel = setInterval(() => this.panelFin.x -= 2.5, 10);
        this.intervalFinScore = setInterval(() => this.panelScore.x -= 2.5, 10);

        setTimeout(() => this.faireStorage(), 7000);

    }

    faireStorage() {

        this.boutonRejouer = true;

        this.records = JSON.parse(localStorage.getItem('records'));

        if (!this.records) {

            this.records = {

                pointage: {
                    points: this.nombreAttrapes
                },

                meilleur: {
                    points: 0
                }

            };

        }

        this.records.pointage.points = this.nombreAttrapes;

        if (this.records.pointage.points > this.records.meilleur.points) {
            this.records.meilleur.points = this.nombreAttrapes;
        }

        localStorage.setItem('records', JSON.stringify(this.records));

        // this.panelFin = new createjs.Bitmap(this.chargeur.getResult('sousMarinPanel'));
        // this.panelFin.x = this.stage.canvas.width / 2 - this.panelFin.getBounds().width / 2;
        // this.panelFin.y = 250;
        // this.stage.addChild(this.panelFin);

        clearInterval(this.intervalFinPanel);
        clearInterval(this.intervalFinScore);

        this.titreFin = new createjs.Text(this.nombreAttrapes, '75px \'bluefish\'', 'white');
        this.titreFin.cache(0, 0, 600, 200);
        this.titreFin.x = 1550;
        this.titreFin.y = 330;
        this.stage.addChild(this.titreFin);

        this.titreMeilleur = new createjs.Text(this.records.meilleur.points, '75px \'bluefish\'', 'white');
        this.titreMeilleur.cache(0, 0, 600, 200);
        this.titreMeilleur.x = 1550;
        this.titreMeilleur.y = 520;
        this.stage.addChild(this.titreMeilleur);

        // bouton pour rejouer
        this.bouton = new createjs.Bitmap(this.chargeur.getResult("bouton"));
        this.bouton.scaleY = 0.4;
        this.bouton.scaleX = 0.4;
        this.bouton.x = 1185;
        this.bouton.y = 650;
        this.stage.addChild(this.bouton);

        this.bouton.addEventListener('click', this.rejouer.bind(this));
    }

    rejouer() {

        // this.stage.removeChild(this.panelFin, this.titreMeilleur, this.titreFin, this.bouton);
        // this.stage.removeChild(this.fond, this.decor3, this.decor2, this.decor1, this.debrisAvion, this.filtre);

        // this.temps = 0;

        // this.ajouterDecors();

        // this.nombreAttrapes = 0;
        // this.textePointage.text = "Objet(s) attrapé(s) : " + this.nombreAttrapes;
        // this.textePointage.updateCache();

        location.reload()
    }

    ajouterDecors() {
        this.fond = new createjs.Sprite(this.chargeur.getResult('fond'));
        this.fond.gotoAndPlay("animations");
        //this.decor3 = new createjs.Bitmap(this.chargeur.getResult('decor3'));
        //this.decor2 = new createjs.Bitmap(this.chargeur.getResult('decor2'));
        //this.decor1 = new createjs.Bitmap(this.chargeur.getResult('decor1'));
        this.filtre = new createjs.Bitmap(this.chargeur.getResult('filtre'));

        //this.stage.addChild(this.fond, this.decor3, this.decor2, this.decor1, this.fond1Element1, this.fond1Element2, this.fond1Element3, this.fond2Element1, this.fond2Element2, this.fond2Element3, this.debrisAvion, this.filtre);
        this.stage.addChild(this.fond, this.filtre);

    }


    fusil() {

        this.fusilventouse = [

            new createjs.Bitmap(this.chargeur.getResult('crossHair1')),
            new createjs.Bitmap(this.chargeur.getResult('crossHair2')),

        ];

        this.stage.addChild(this.fusilventouse[0], this.fusilventouse[1]);

        for (let z = 0; z < 2; z++) {
            this.fusilventouse[z].scaleX = 0.5;
            this.fusilventouse[z].scaleY = 0.5;
            this.fusilventouse[z].regX  = this.fusilventouse[z].getBounds().width / 2;
            this.fusilventouse[z].regY  = this.fusilventouse[z].getBounds().height / 2;
        }

        this.ventouseCompletDroit = [
            new createjs.Bitmap(this.chargeur.getResult('bouteille')),
            new BatonD(this.chargeur, this.conteneurBoites, this.conteneurBoites2, this.conteneurBombes, this.conteneurBombes2, this.conteneurPoissons, this.conteneurPoissons2)
        ];

        this.ventouseCompletGauche = [
            new createjs.Bitmap(this.chargeur.getResult('bouteille')),
            new BatonG(this.chargeur, this.conteneurBoites, this.conteneurBoites2, this.conteneurBombes, this.conteneurBombes2, this.conteneurPoissons, this.conteneurPoissons2)
        ];

        //----bouteille
        this.ventouseCompletDroit[0].x = 1255;
        this.ventouseCompletDroit[0].y = 1130;

        this.ventouseCompletDroit[0].regX = 60;
        this.ventouseCompletDroit[0].regY = 140;

        //----bouteille
        this.ventouseCompletGauche[0].y = 1130;
        this.ventouseCompletGauche[0].x = 600;

        this.ventouseCompletGauche[0].regX = 60;
        this.ventouseCompletGauche[0].regY = 140;

        this.stage.addChild(this.ventouseCompletDroit[1], this.ventouseCompletGauche[1], this.ventouseCompletDroit[0], this.ventouseCompletGauche[0]);

        this.ventouseCompletGauche[1].ventouse();
        //this.ventouseCompletGauche[1].ventouse(this.chargeur, this.conteneurBoites, this.conteneurBombes, this.conteneurPoissons);
        //this.ventouseCompletDroit[1].ventouse(this.chargeur, this.conteneurBoites, this.conteneurBombes, this.conteneurPoissons);
        this.ventouseCompletDroit[1].ventouse();

        setInterval(() => {

            if (Math.sign(navigator.getGamepads()[1].axes[0]) <= 0) {
                this.fusilventouse[1].x = (navigator.getGamepads()[1].axes[0] * 900) + 900;
                if (this.peutTirerBlue === true) {
                    this.angleFusilG();
                }
            }

            else {
                this.fusilventouse[1].x = (navigator.getGamepads()[1].axes[0] * 900) + 900;
                if (this.peutTirerBlue === true) {
                    this.angleFusilG();
                }
            }

            if (Math.sign(navigator.getGamepads()[1].axes[1]) <= 0) {
                this.fusilventouse[1].y = (navigator.getGamepads()[1].axes[1] * 450) + 450;
            }
            else {
                this.fusilventouse[1].y = (navigator.getGamepads()[1].axes[1] * 450) + 450;
            }

            //Autre fusil

            if (Math.sign(navigator.getGamepads()[0].axes[0]) <= 0) {
                this.fusilventouse[0].x = (navigator.getGamepads()[0].axes[0] * 900) + 900;
                if (this.peutTirerRed === true) {
                    this.angleFusilD();
                }
            }

            else {
                this.fusilventouse[0].x = (navigator.getGamepads()[0].axes[0] * 900) + 900;
                if (this.peutTirerRed === true) {
                    this.angleFusilD();
                }
            }

            if (Math.sign(navigator.getGamepads()[0].axes[1]) <= 0) {
                this.fusilventouse[0].y = (navigator.getGamepads()[0].axes[1] * 450) + 450;
            }
            else {
                this.fusilventouse[0].y = (navigator.getGamepads()[0].axes[1] * 450) + 450;
            }

        }, 0);

        this.tireGunRed(this.bombe, this.boite, this.poisson)
        this.tireGunBlue(this.bombe, this.boite, this.poisson)

    }

    angleFusilD() {

        let XdistanceBouD = this.ventouseCompletDroit[0].x - this.fusilventouse[0].x;
        let YdistanceBouD = this.ventouseCompletDroit[0].y - this.fusilventouse[0].y;
        this.ventouseCompletDroit[0].rotation = (Math.atan2(YdistanceBouD, XdistanceBouD) * 180 / Math.PI) - 90;

        let XdistanceBatD = this.ventouseCompletDroit[1].x - this.fusilventouse[0].x;
        let YdistanceBatD = this.ventouseCompletDroit[1].y - this.fusilventouse[0].y;
        this.ventouseCompletDroit[1].rotation = (Math.atan2(YdistanceBatD, XdistanceBatD) * 180 / Math.PI) - 90;

        // this.endroitVentouse1 = 1255 - 35;
        //
        // if (this.fusilventouse[0].x < this.endroitVentouse1 - 85) {
        //     this.ventouseCompletDroit[0].x = 1255;
        //     this.a = this.endroitVentouse1 - this.fusilventouse[0].x - 40;
        //     this.b = 1080 - this.fusilventouse[0].y;
        //     this.c = Math.sqrt((this.a * this.a) + (this.b * this.b));
        //     // Angle en radians
        //     this.rAngle = Math.cos(this.a / this.c);
        //     // Angle en degrés
        //     this.dAngle = (180 * this.rAngle / Math.PI) - 90;
        //     this.ventouseCompletDroit[1].rotation = this.dAngle;
        //     this.ventouseCompletDroit[0].rotation = this.dAngle;
        // } else if (this.fusilventouse[0].x > this.endroitVentouse1 + 65) {
        //     this.ventouseCompletDroit[0].x = 1220;
        //     this.a = this.fusilventouse[0].x - this.endroitVentouse1 - 40;
        //     this.b = 1080 - this.fusilventouse[0].y;
        //     this.c = Math.sqrt((this.a * this.a) + (this.b * this.b));
        //     // Angle en radians
        //     this.rAngle = Math.cos(this.b / this.c);
        //     // Angle en degrés
        //     this.dAngle = 180 * this.rAngle / Math.PI;
        //     this.ventouseCompletDroit[1].rotation = this.dAngle;
        //     this.ventouseCompletDroit[0].rotation = this.dAngle;
        // } else if(this.fusilventouse[0].x > this.endroitVentouse1 - 85 && this.fusilventouse[0].x < this.endroitVentouse1){
        //     this.hautDessus = this.fusilventouse[0].x - 1133;
        //     this.dAngle = -30 + this.hautDessus/3;
        //     this.ventouseCompletDroit[1].rotation = this.dAngle;
        //     this.ventouseCompletDroit[0].rotation = this.dAngle;
        // }
        // else{
        //     this.hautDessus = this.fusilventouse[0].x - 1133;
        //     this.dAngle = -30 + this.hautDessus/3;
        //     this.ventouseCompletDroit[1].rotation = this.dAngle;
        //     this.ventouseCompletDroit[0].rotation = this.dAngle;
        // }

    }

    angleFusilG() {

        let XdistanceBouG = this.ventouseCompletGauche[0].x - this.fusilventouse[1].x;
        let YdistanceBouG = this.ventouseCompletGauche[0].y - this.fusilventouse[1].y;
        this.ventouseCompletGauche[0].rotation = (Math.atan2(YdistanceBouG, XdistanceBouG) * 180 / Math.PI) - 90;

        let XdistanceBatG = this.ventouseCompletGauche[1].x - this.fusilventouse[1].x;
        let YdistanceBatG = this.ventouseCompletGauche[1].y - this.fusilventouse[1].y;
        this.ventouseCompletGauche[1].rotation = (Math.atan2(YdistanceBatG, XdistanceBatG) * 180 / Math.PI) - 90;

        // this.endroitVentouse2 = 600 - 30;
        //
        // if (this.fusilventouse[1].x < this.endroitVentouse2 - 65) {
        //     this.ventouseCompletGauche[0].x = 600;
        //     this.a = this.endroitVentouse2 - this.fusilventouse[1].x - 40;
        //     this.b = 1080 - this.fusilventouse[1].y;
        //     this.c = Math.sqrt((this.a * this.a) + (this.b * this.b));
        //     // Angle en radians
        //     this.rAngle = Math.cos(this.a / this.c);
        //     // Angle en degrés
        //     this.dAngle = (180 * this.rAngle / Math.PI) - 90;
        //     this.ventouseCompletGauche[1].rotation = this.dAngle;
        //     this.ventouseCompletGauche[0].rotation = this.dAngle;
        // } else if (this.fusilventouse[1].x > this.endroitVentouse2 + 85) {
        //     this.ventouseCompletGauche[0].x = 555;
        //     this.a = this.fusilventouse[1].x - this.endroitVentouse2 - 40;
        //     this.b = 1080 - this.fusilventouse[1].y;
        //     this.c = Math.sqrt((this.a * this.a) + (this.b * this.b));
        //     // Angle en radians
        //     this.rAngle = Math.cos(this.b / this.c);
        //     // Angle en degrés
        //     this.dAngle = 180 * this.rAngle / Math.PI;
        //     this.ventouseCompletGauche[1].rotation = this.dAngle;
        //     this.ventouseCompletGauche[0].rotation = this.dAngle;
        // } else if(this.fusilventouse[1].x > this.endroitVentouse2 - 65 && this.fusilventouse[1].x < this.endroitVentouse2){
        //     this.hautDessus = this.fusilventouse[1].x - 500;
        //     this.dAngle = -30 + this.hautDessus/3;
        //     this.ventouseCompletGauche[1].rotation = this.dAngle;
        //     this.ventouseCompletGauche[0].rotation = this.dAngle;
        // }
        // else{
        //     this.hautDessus = this.fusilventouse[1].x - 500;
        //     this.dAngle = -30 + this.hautDessus/3;
        //     this.ventouseCompletGauche[1].rotation = this.dAngle;
        //     this.ventouseCompletGauche[0].rotation = this.dAngle;
        // }

    }

    //this.ventouseCompletDroit[1].gotoAndStop("tendu");

    actualiser() {



        //mettre sur le dessus
        this.stage.setChildIndex(this.filtre, (this.stage.numChildren - 1));
        this.stage.setChildIndex(this.fusilventouse[0], (this.stage.numChildren - 1));
        this.stage.setChildIndex(this.fusilventouse[1], (this.stage.numChildren - 1));

        if(this.filtreRouge){
            this.stage.setChildIndex(this.filtreRouge, (this.stage.numChildren - 1));
        }

        if(this.danger){
            this.stage.setChildIndex(this.danger, (this.stage.numChildren - 1));
        }

        // bombes qui bougent
        this.conteneurBombes.children.forEach(function (e) {
            e.x += 4;

            if (e.x > 1920) {
                e.parent.removeChild(e);
            }
        });
        this.stage.setChildIndex(this.conteneurBombes, (this.stage.numChildren - 2));

        // bombes2 qui bougent
        this.conteneurBombes2.children.forEach(function (e) {
            e.x -= 4;

            if (e.x < -600) {
                e.parent.removeChild(e);
            }
        });
        this.stage.setChildIndex(this.conteneurBombes2, (this.stage.numChildren - 2));


        // boites qui bougent
        this.conteneurBoites.children.forEach(function (e) {
            e.x += 6;

            if (e.x > 1920) {
                e.parent.removeChild(e);
            }
        });

        // boites2 qui bougent
        this.conteneurBoites2.children.forEach(function (e) {
            e.x -= 6;

            if (e.x < -1000) {
                e.parent.removeChild(e);
            }
        });

        // poissons qui bougent
        this.conteneurPoissons.children.forEach(function (e) {
            e.x += 4.5;

            if (e.x > 1920) {
                e.parent.removeChild(e);
            }
        });

        // poissons2 qui bougent
        this.conteneurPoissons2.children.forEach(function (e) {
            e.x -= 4.5;

            if (e.x < -300) {
                e.parent.removeChild(e);
            }
        });

        this.ventouseCompletGauche[1].bouge(this.ventouseCompletGauche[1].rotation);
        this.ventouseCompletDroit[1].bouge(this.ventouseCompletDroit[1].rotation);

    }

    actuTickColRed() {

        this.peutTirerRed = false;

        //---------------------------------------------------------------------------Droite

        //----------------------------------------Bouton recommencer

        if (this.boutonRejouer === true){

            let bRec = this.bouton;

            let intersectionBout = ndgmr.checkPixelCollision(this.fusilventouse[0], bRec);

            if (intersectionBout) {

                this.rejouer()

            }

        }

        //----------------------------------------Boîtes

        if (this.colRed === false) {

            for (let x = 0; x < this.conteneurBoites.numChildren; x++) {

                let boite = this.conteneurBoites.getChildAt(x);

                let intersectionBoite = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, boite);

                if (intersectionBoite) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);

                    this.tween1.paused = true;

                    // this.ventouseCompletDroit[1].ventou.stick();

                    boite.replacerBoiteSurVentouse(this.ventouseCompletDroit[1]);

                    createjs.Tween
                        .get(boite)
                        .wait(350)
                        .call(() => boite.enleverBoite(this.ventouseCompletDroit[1].ventou))
                        .call(() => this.nombreAttrapes += 1)
                        .call(() => this.textePointage.text = this.nombreAttrapes)
                        .call(() => this.textePointage.updateCache());

                    this.ventouseCompletDroit[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletDroit[1])
                        .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y}, 350)
                        .call(() => this.tireGunRed());

                }

            }

        }

        //----------------------------------------Boîtes2

        if (this.colRed === false) {

            for (let x = 0; x < this.conteneurBoites2.numChildren; x++) {

                let boite = this.conteneurBoites2.getChildAt(x);

                let intersectionBoite = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, boite);

                if (intersectionBoite) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);

                    this.tween1.paused = true;

                    // this.ventouseCompletDroit[1].ventou.stick();

                    boite.replacerBoiteSurVentouse(this.ventouseCompletDroit[1]);

                    createjs.Tween
                        .get(boite)
                        .wait(350)
                        .call(() => boite.enleverBoite(this.ventouseCompletDroit[1].ventou))
                        .call(() => this.nombreAttrapes += 1)
                        .call(() => this.textePointage.text = this.nombreAttrapes)
                        .call(() => this.textePointage.updateCache());

                    this.ventouseCompletDroit[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletDroit[1])
                        .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y}, 350)
                        .call(() => this.tireGunRed());

                }

            }

        }

        //----------------------------------------Poissons

        if (this.colRed === false) {

            for (let x = 0; x < this.conteneurPoissons.numChildren; x++) {

                let poiss = this.conteneurPoissons.getChildAt(x);

                let intersectionPoiss = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, poiss);

                if (intersectionPoiss) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);

                    this.tween1.paused = true;

                    //  this.ventouseCompletDroit[1].ventou.stick();

                    poiss.replacerPoissSurVentouse(this.ventouseCompletDroit[1]);

                    createjs.Tween
                        .get(poiss)
                        .wait(351)
                        .call(() => poiss.enleverPoisson(this.ventouseCompletDroit[1].ventou));

                    this.ventouseCompletDroit[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletDroit[1])
                        .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y}, 350)
                        .call(() => this.tireGunRed());


                }

            }

        }

        //----------------------------------------Poissons2

        if (this.colRed === false){

            for (let x = 0; x < this.conteneurPoissons2.numChildren; x++) {

                let poiss = this.conteneurPoissons2.getChildAt(x);

                let intersectionPoiss = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, poiss);

                if (intersectionPoiss) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);

                    this.tween1.paused = true;

                    //   this.ventouseCompletDroit[1].ventou.stick();

                    poiss.replacerPoissSurVentouse(this.ventouseCompletDroit[1]);

                    createjs.Tween
                        .get(poiss)
                        .wait(351)
                        .call(() => poiss.enleverPoisson(this.ventouseCompletDroit[1].ventou));

                    this.ventouseCompletDroit[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletDroit[1])
                        .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y}, 350)
                        .call(() => this.tireGunRed());


                }

            }

        }

        //----------------------------------------Bombes

        for (let x = 0; x < this.conteneurBombes.numChildren; x++) {

            let bombe = this.conteneurBombes.getChildAt(x);

            let intersectionBombe = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, bombe);

            if (intersectionBombe) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);
                createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                this.tween1.paused = true;
                this.tween2.paused = true;

                bombe.enleverBombe();

                // filtre rouge DANGER
                this.filtreRouge = new createjs.Bitmap(this.chargeur.getResult('filtreRouge'));
                this.stage.addChild(this.filtreRouge);
                this.danger = new createjs.Bitmap(this.chargeur.getResult('danger'));
                this.danger.x = this.stage.canvas.width/2 - this.danger.getBounds().width/2;
                this.danger.y = this.stage.canvas.height/2 - this.danger.getBounds().height/ 2;
                this.stage.addChild(this.danger);

                createjs.Tween
                    .get(this.danger)
                    .wait(4000)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut);

                createjs.Tween
                    .get(this.filtreRouge)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut)
                    .call(()=>this.stage.removeChild(this.filtreRouge, this.danger));

                createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y});

                createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y})
                    .call(() => this.brisee());

            }

        }

        //----------------------------------------Bombes2

        for (let x = 0; x < this.conteneurBombes2.numChildren; x++) {

            let bombe = this.conteneurBombes2.getChildAt(x);

            let intersectionBombe = ndgmr.checkPixelCollision(this.ventouseCompletDroit[1].ventou, bombe);

            if (intersectionBombe) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);
                createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                this.tween1.paused = true;
                this.tween2.paused = true;

                bombe.enleverBombe();

                // filtre rouge DANGER
                this.filtreRouge = new createjs.Bitmap(this.chargeur.getResult('filtreRouge'));
                this.stage.addChild(this.filtreRouge);
                this.danger = new createjs.Bitmap(this.chargeur.getResult('danger'));
                this.danger.x = this.stage.canvas.width/2 - this.danger.getBounds().width/2;
                this.danger.y = this.stage.canvas.height/2 - this.danger.getBounds().height/ 2;
                this.stage.addChild(this.danger);

                createjs.Tween
                    .get(this.danger)
                    .wait(4000)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut);

                createjs.Tween
                    .get(this.filtreRouge)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut)
                    .call(()=>this.stage.removeChild(this.filtreRouge, this.danger));

                createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y});

                createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y})
                    .call(() => this.brisee());

            }

        }

    }

    actuTickColBlue() {

        this.peutTirerBlue = false;

        //---------------------------------------------------------------------------Droite

        //----------------------------------------Bouton recommencer

        if (this.boutonRejouer === true){

            let bRec = this.bouton;

            let intersectionBout = ndgmr.checkPixelCollision(this.fusilventouse[1], bRec);

            if (intersectionBout) {

                this.rejouer()

            }

        }

        //----------------------------------------Boîtes

        if (this.colBlue === false){

            for (let x = 0; x < this.conteneurBoites.numChildren; x++) {

                let boite = this.conteneurBoites.getChildAt(x);

                let intersectionBoite = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, boite);

                if (intersectionBoite) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                    this.tween2.paused = true;

                    // this.ventouseCompletGauche[1].ventou.stick();

                    boite.replacerBoiteSurVentouse(this.ventouseCompletGauche[1]);

                    createjs.Tween
                        .get(boite)
                        .wait(350)
                        .call(() => boite.enleverBoite(this.ventouseCompletGauche[1].ventou))
                        .call(() => this.nombreAttrapes += 1)
                        .call(() => this.textePointage.text = this.nombreAttrapes)
                        .call(() => this.textePointage.updateCache());

                    this.ventouseCompletGauche[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletGauche[1])
                        .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y}, 350)
                        .call(() => this.tireGunBlue());

                }

            }

        }

        //----------------------------------------Boîtes2

        if (this.colBlue === false){

            for (let x = 0; x < this.conteneurBoites2.numChildren; x++) {

                let boite = this.conteneurBoites2.getChildAt(x);

                let intersectionBoite = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, boite);

                if (intersectionBoite) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                    this.tween2.paused = true;

                    // this.ventouseCompletGauche[1].ventou.stick();

                    boite.replacerBoiteSurVentouse(this.ventouseCompletGauche[1]);

                    createjs.Tween
                        .get(boite)
                        .wait(350)
                        .call(() => boite.enleverBoite(this.ventouseCompletGauche[1].ventou))
                        .call(() => this.nombreAttrapes += 1)
                        .call(() => this.textePointage.text = this.nombreAttrapes)
                        .call(() => this.textePointage.updateCache());

                    this.ventouseCompletGauche[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletGauche[1])
                        .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y}, 350)
                        .call(() => this.tireGunBlue());

                }

            }

        }



        //----------------------------------------Poissons

        if (this.colBlue === false){

            for (let x = 0; x < this.conteneurPoissons.numChildren; x++) {

                let poiss = this.conteneurPoissons.getChildAt(x);

                let intersectionPoiss = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, poiss);

                if (intersectionPoiss) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                    this.tween2.paused = true;

                    //this.ventouseCompletGauche[1].ventou.stick();

                    poiss.replacerPoissSurVentouse(this.ventouseCompletGauche[1]);

                    createjs.Tween
                        .get(poiss)
                        .wait(351)
                        .call(() => poiss.enleverPoisson(this.ventouseCompletGauche[1].ventou));

                    this.ventouseCompletGauche[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletGauche[1])
                        .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y}, 350)
                        .call(() => this.tireGunBlue());


                }

            }

        }

        //----------------------------------------Poissons2

        if (this.colBlue === false){

            for (let x = 0; x < this.conteneurPoissons2.numChildren; x++) {

                let poiss = this.conteneurPoissons2.getChildAt(x);

                let intersectionPoiss = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, poiss);

                if (intersectionPoiss) {

                    createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                    this.tween2.paused = true;

                    //this.ventouseCompletGauche[1].ventou.stick();

                    poiss.replacerPoissSurVentouse(this.ventouseCompletGauche[1]);

                    createjs.Tween
                        .get(poiss)
                        .wait(351)
                        .call(() => poiss.enleverPoisson(this.ventouseCompletGauche[1].ventou));

                    this.ventouseCompletGauche[1].gotoAndStop("tendu");

                    createjs.Tween
                        .get(this.ventouseCompletGauche[1])
                        .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y}, 350)
                        .call(() => this.tireGunBlue());


                }

            }

        }

        //----------------------------------------Bombes

        for (let x = 0; x < this.conteneurBombes.numChildren; x++) {

            let bombe = this.conteneurBombes.getChildAt(x);

            let intersectionBombe = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, bombe);

            if (intersectionBombe) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);
                createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                this.tween2.paused = true;
                this.tween1.paused = true;

                bombe.enleverBombe();

                // filtre rouge DANGER
                this.filtreRouge = new createjs.Bitmap(this.chargeur.getResult('filtreRouge'));
                this.stage.addChild(this.filtreRouge);
                this.danger = new createjs.Bitmap(this.chargeur.getResult('danger'));
                this.danger.x = this.stage.canvas.width/2 - this.danger.getBounds().width/2;
                this.danger.y = this.stage.canvas.height/2 - this.danger.getBounds().height/ 2;
                this.stage.addChild(this.danger);

                createjs.Tween
                    .get(this.danger)
                    .wait(4000)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut);

                createjs.Tween
                    .get(this.filtreRouge)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut)
                    .call(()=>this.stage.removeChild(this.filtreRouge, this.danger));

                createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y});

                createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y})
                    .call(() => this.brisee());

            }

        }

        //----------------------------------------Bombes2

        for (let x = 0; x < this.conteneurBombes2.numChildren; x++) {

            let bombe = this.conteneurBombes2.getChildAt(x);

            let intersectionBombe = ndgmr.checkPixelCollision(this.ventouseCompletGauche[1].ventou, bombe);

            if (intersectionBombe) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);
                createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                this.tween2.paused = true;
                this.tween1.paused = true;

                bombe.enleverBombe();

                // filtre rouge DANGER
                this.filtreRouge = new createjs.Bitmap(this.chargeur.getResult('filtreRouge'));
                this.stage.addChild(this.filtreRouge);
                this.danger = new createjs.Bitmap(this.chargeur.getResult('danger'));
                this.danger.x = this.stage.canvas.width/2 - this.danger.getBounds().width/2;
                this.danger.y = this.stage.canvas.height/2 - this.danger.getBounds().height/ 2;
                this.stage.addChild(this.danger);

                createjs.Tween
                    .get(this.danger)
                    .wait(4000)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut);

                createjs.Tween
                    .get(this.filtreRouge)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0.3}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 1}, 1000, createjs.Ease.quadInOut)
                    .to({alpha: 0}, 1000, createjs.Ease.quadInOut)
                    .call(()=>this.stage.removeChild(this.filtreRouge, this.danger));

                createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y});

                createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y})
                    .call(() => this.brisee());

            }

        }

    }

    brisee() {

        this.colBlue = true;
        this.colRed = true;

        this.ventouseCompletDroit[1].alpha = 0;
        this.ventouseCompletDroit[1].ventou.alpha = 0;

        this.ventouseCompletGauche[1].alpha = 0;
        this.ventouseCompletGauche[1].ventou.alpha = 0;

        this.peutTirerBlue = false;
        this.peutTirerRed = false;

        gamepad.off('press', 'button_1');
        gamepad2.off('press', 'button_2');

        setTimeout(this.retablir.bind(this), 5000);

    }

    retablir() {

        this.colBlue = false;
        this.colRed = false;

        //----------------------------------------------------

        this.ventouseCompletDroit[1].alpha = 1;
        this.ventouseCompletDroit[1].ventou.alpha = 1;

        this.eActuTickColRed = this.actuTickColRed.bind(this);

        if (this.colBlue === false && this.colRed === false){

            this.peutTirerRed = true;

            gamepad2.on('press', 'button_2', () => {

                this.onetimeRed = false;

                this.peutTirerRed = false;

                this.posXRed = this.fusilventouse[0].x + 200;
                this.posYRed = this.fusilventouse[0].y + 200;

                this.tween1 = createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.posXRed, y: this.posYRed}, 750)
                    .call(() => this.aucuneCollRed())

                createjs.Ticker.addEventListener('tick', this.eActuTickColRed);

                gamepad2.off('press', 'button_2');

                this.ventouseCompletDroit[1].gotoAndPlay("wiggle");

                createjs.Sound.play("echo", {"volume": 1});

            });

        }

        //---------------------------------------------

        this.ventouseCompletGauche[1].alpha = 1;
        this.ventouseCompletGauche[1].ventou.alpha = 1;

        this.eActuTickColBlue = this.actuTickColBlue.bind(this);

        if (this.colBlue === false && this.colRed === false){

            this.peutTirerBlue = true;

            gamepad.on('press', 'button_1', () => {

                this.onetimeBlue = false;

                this.peutTirerBlue = false;

                this.posXBlue = this.fusilventouse[1].x + 200;
                this.posYBlue = this.fusilventouse[1].y + 200;

                this.tween2 = createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.posXBlue, y: this.posYBlue}, 750)
                    .call(() => this.aucuneCollBlue())

                createjs.Ticker.addEventListener('tick', this.eActuTickColBlue);

                gamepad.off('press', 'button_1');

                this.ventouseCompletGauche[1].gotoAndPlay("wiggle");

                createjs.Sound.play("echo", {"volume": 1});

            });

        }

    }

    aucuneCollRed() {

        if (this.colBlue === false && this.colRed === false){

            if (this.ventouseCompletDroit[1].x == this.posXRed && this.ventouseCompletDroit[1].y == this.posYRed) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColRed);

                this.ventouseCompletDroit[1].gotoAndStop("tendu");

                createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.ventouseCompletDroit[0].x, y: this.ventouseCompletDroit[0].y}, 350)
                    .call(() => this.tireGunRed());


            }

        }

    }

    aucuneCollBlue() {

        if (this.colBlue === false && this.colRed === false){

            if (this.ventouseCompletGauche[1].x == this.posXBlue && this.ventouseCompletGauche[1].y == this.posYBlue) {

                createjs.Ticker.removeEventListener('tick', this.eActuTickColBlue);

                this.ventouseCompletGauche[1].gotoAndStop("tendu");

                createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.ventouseCompletGauche[0].x, y: this.ventouseCompletGauche[0].y}, 350)
                    .call(() => this.tireGunBlue());

            }

        }

    }

    tireGunRed() {

        this.eActuTickColRed = this.actuTickColRed.bind(this);

        if (!this.onetimeRed) {
            this.gachetteRed();
            this.onetimeRed = true;
        }

    }

    gachetteRed(){

        if (this.colBlue === false && this.colRed === false){

            this.peutTirerRed = true;

            gamepad2.on('press', 'button_2', () => {

                this.onetimeRed = false;

                this.peutTirerRed = false;

                this.posXRed = this.fusilventouse[0].x;
                this.posYRed = this.fusilventouse[0].y;

                this.tween1 = createjs.Tween
                    .get(this.ventouseCompletDroit[1])
                    .to({x: this.posXRed, y: this.posYRed}, 750)
                    .call(() => this.aucuneCollRed())

                createjs.Ticker.addEventListener('tick', this.eActuTickColRed);

                gamepad2.off('press', 'button_2');

                this.ventouseCompletDroit[1].gotoAndPlay("wiggle");

                createjs.Sound.play("echo", {"volume": 1});

            });

        }

    }

    tireGunBlue() {

        this.eActuTickColBlue = this.actuTickColBlue.bind(this);

        if (!this.onetimeBlue) {
            this.gachetteBlue();
            this.onetimeBlue = true;
        }

    }

    gachetteBlue(){

        if (this.colBlue === false && this.colRed === false){

            this.peutTirerBlue = true;

            gamepad.on('press', 'button_1', () => {

                this.onetimeBlue = false;

                this.peutTirerBlue = false;

                this.posXBlue = this.fusilventouse[1].x;
                this.posYBlue = this.fusilventouse[1].y;

                this.tween2 = createjs.Tween
                    .get(this.ventouseCompletGauche[1])
                    .to({x: this.posXBlue, y: this.posYBlue}, 750)
                    .call(() => this.aucuneCollBlue())

                createjs.Ticker.addEventListener('tick', this.eActuTickColBlue);

                gamepad.off('press', 'button_1');

                this.ventouseCompletGauche[1].gotoAndPlay("wiggle");

                createjs.Sound.play("echo", {"volume": 1});

            });

        }

    }

}