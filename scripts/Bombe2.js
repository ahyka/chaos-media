export default class Bombe2 extends createjs.Sprite{
    constructor(chargeur){
        super(chargeur.getResult('bombe'));

        this.gotoAndStop("8");

        this.taille = Math.random()*0.4+0.2;

        this.scaleY = this.taille;
        this.scaleX = this.taille;

        this.x = 2400;
        this.y = Math.random()*400;

        createjs.Tween
            .get(this)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y+50}, 2000, createjs.Ease.quadInOut)
            .to({y: this.y-50}, 2000, createjs.Ease.quadInOut);

    }

    enleverBombe(){

        createjs.Sound.play("alarme", {"volume": 1});

        this.gotoAndPlay("visuel_bombe_explision_sprite");

        createjs.Tween
            .get(this)
            .wait(1000)
            .call(function () {
                this.parent.removeChild(this);
            })

    }
}