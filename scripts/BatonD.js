import VentouseD from "./VentouseD.js";

export default class BatonD extends createjs.Sprite{
    constructor(chargeur, conteneurBoites, conteneurBombes, conteneurPoissons, conteneurPoissons2){
        super(chargeur.getResult("baton"));

        this.x = 1255;
        this.y = 1100;

        this.regX = 435;
        this.regY = 250;

        this.leChargeur = chargeur;
        this.leConteneurBoites = conteneurBoites;
        this.leConteneurBombes = conteneurBombes;
        this.leConteneurPoissons = conteneurPoissons;
        this.leConteneurPoissons2 = conteneurPoissons2;

        this.gotoAndStop("tendu");

    }

    ventouse(){

        this.ventou = new VentouseD(this.leChargeur, this.leConteneurBoites, this.leConteneurBombes, this.leConteneurPoissons, this.leConteneurPoissons2);

        let index = this.stage.getChildIndex(this);
        this.stage.addChildAt(this.ventou, index + 3);

    }

    bouge(angle){

        this.lAngle = angle;

        this.ventou.x = this.x;
        this.ventou.y = this.y;

        this.ventou.regX = 469;
        this.ventou.regY = 717;

        this.ventou.rotation = this.lAngle;
    }

}